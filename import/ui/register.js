import './register.html';

Template.register.events({
    'submit form': function (event) {
        event.preventDefault();

        var email = event.target.email.value;
        var firstName = event.target.firstName.value;
        var lastName = event.target.lastName.value;
        var password = event.target.password.value;
        var passwordAgain = event.target.passwordAgain.value;

        // Trim function
        function trimInput(val){
            return val.replace(/^\s*|\s*$/g, "");
        }

        var email = trimInput(email);

        // Check password 
        var isValidPassword = function(pwd, pwd2){
            if(pwd === pwd2){
                return pwd.length >= 6 ? true : false;
            }else{
                return swal({
                    title: "passwords don't match",
                    text: "Please try again",
                    showConfirmButton: true,
                    type: "error"
                });
            }
        }

        if(isValidPassword(password, passwordAgain)){
            Accounts.createUser({
                email: email,
                firstName: firstName,
                lastName: lastName,
                password: password
            }, function (error) {
                if (error) {
                    return swal({
                        title: error.reason,
                        text: "please try again",
                        timer: 9000,
                        showConfirmbutton: false,
                        type: "error"
                    });
                } else {
                    FlowRouter.go('/');
                }
            });
        }
        return false;
    },
    'click .goToLogin'(){
        FlowRouter.go('/login');
    },
})