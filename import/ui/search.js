import './search.html';
import { Userinfo } from "../api/userinfo.js";
import { Template } from 'meteor/templating';

// Search event on changing text
Template.search.events({
  'keyup input.search-query': function (evt) {
    Session.set("search-query", evt.currentTarget.value);
  },
})

// <------------------------>

// This is the table template, which takes input from search bar
// And compares with db- entries

Template.people.helpers({
  searchResults(){
      var keyword  = Session.get("search-query");
      var query = new RegExp( keyword, 'i' );
      var results
      if(query.test("/^(?!\s*$).+/") === false)
      {
        results = Userinfo.find( { $or: [{'title': query},
        {'device': query},
        {'country': query   },] } );
      }
      return {results: results};
  },
})

// Seach Helper
/*
Template.searchHelper.onCreated(function onCreated(){
  this.hoverstate = new ReactiveVar(false);
})

Template.searchHelper.helpers({
  showHover(){
    return Template.instance().hoverstate.get();
  }
})

Template.searchHelper.events({
  'mouseenter .hoverMethod'(event, instance){
    instance.hoverstate.set(true);
  },
  'mouseleave .hoverMethod'(event, instance){
    instance.hoverstate.set(false);
  }
})*/