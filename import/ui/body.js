import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import './body.html';
import './coreUi.js'; // coreUI elements

// Subscribe to the database!
Template.body.created = function(){
    Meteor.subscribe('userinfo');
    Meteor.subscribe('neighbourInfo');
}
