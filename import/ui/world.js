import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4geodata_worldIndiaLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import am4geodata_indiaLow from "@amcharts/amcharts4-geodata/india2020Low";
import { Template } from "meteor/templating";
import { Userinfo } from "../api/userinfo.js";
import { NeighbourInfo } from "../api/userinfo.js";
import './world.html';
import * as crg from "country-reverse-geocoding"; // Used to get country name from Co-ordinates
import "./search.js";
import { Meteor } from 'meteor/meteor';
import { Session } from "meteor/session";
import { round } from "@amcharts/amcharts4/.internal/core/utils/Math";

// World Template 
am4core.useTheme(am4themes_animated);

Template.world.onRendered(function(){
    var instance = this;
    instance.autorun(function(){
        // Auto dispose
        am4core.options.autoDispose = true;
        // Creating chart instance
        var chart = am4core.create("chartdiv", am4maps.MapChart);
        
        //toggle button to switch india/world
        var button = chart.createChild(am4core.SwitchButton);
        button.align = "right";
        button.marginTop = 40;
        button.marginRight = 40;
        button.valign = "top";
        button.leftLabel.text = "India";
        button.rightLabel.text = "World";
        button.leftLabel.fill = am4core.color("#fff");
        button.rightLabel.fill = am4core.color("#fff");
        button.isActive = false;

        button.events.on("toggled", function() {
            FlowRouter.go("/");
            if (button.isActive) {
                // goHome() resets the zoom
                chart.goHome();
                // Adding geodata based on the map we want
                // Since geodata changes, autorun() re-renders it
                chart.geodata = am4geodata_worldIndiaLow;
            }
            else {
                chart.goHome();
                chart.geodata = am4geodata_indiaLow;
            }
        })
        
        
        // getting DB data
        const mapdata = Userinfo.find({});
        var jsond = [];
        var lined = [];
        // Simplify data to object format
        mapdata.forEach((post) => {
            jsond.push({
                title: post.title,
                longitude: parseFloat(post.longitude),
                latitude: parseFloat(post.latitude)
            });
            var neighbourdata = NeighbourInfo.find({userID: post._id}).fetch()[0].neighbour;

            neighbourdata.forEach((item) => {
              let a = Userinfo.find({_id: item}).fetch();
              if(post._id > a[0]._id)
              {
                var _id = post._id + a[0]._id;
                var name = post.title + " to " + a[0].title;
              }
              else{
                var _id  = a[0]._id + post._id;
                var name = a[0].title + " to " + post.title;
              }
              lined.push(
                {
                  "name": name,
                  "_id": _id,
                  "multiGeoLine": [
                    [
                      {"latitude": parseFloat(round(post.latitude, 6)), "longitude": parseFloat(round(post.longitude, 6))},
                      {"latitude": parseFloat(round(a[0].latitude, 6)), "longitude": parseFloat(round(a[0].longitude, 6))}
                    ]
                  ]
                }
              )
            })
        });

        // Set map definition
        chart.geodata = am4geodata_indiaLow;

        // Set projection
        chart.projection = new am4maps.projections.Miller();

        // Disable pan
        chart.seriesContainer.draggable = false;
        chart.seriesContainer.resizable = false;

        // Disbale right click on chart
        chart.contextMenuDisabled = true;
        // Disable Default right click **
        chart.seriesContainer.contextMenuDisabled = true;

        // configuring zoom
        // set to one in dev, will change
        chart.maxZoomLevel = 1;

        // Add zoom control
        chart.zoomControl = new am4maps.ZoomControl();

        // Create map polygon series
        var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

        // Exclude Antartica
        polygonSeries.exclude = ["AQ"];

        // Make map load polygon (like country names) data from GeoJSON
        polygonSeries.useGeodata = true;


        // Configure series
        var polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.tooltipText = "{name}";
        polygonTemplate.align = "center";
        polygonTemplate.width = "auto";
        polygonTemplate.fill = am4core.color("#47c78a");

        // events on polygonTemplate
        polygonTemplate.events.on("hit", function(ev) {
            var data = chart.svgPointToGeo(ev.svgPoint);
            // Important: Was adding a logic to verify that addUser doesn't add anyexisting point,
            // but by logic existing points are a layer above and they can't be clicked polygon Series!!
            FlowRouter.go("/addUser?latitude="+ data.latitude.toString() + "&longitude=" + data.longitude.toString());
        });
        
        // Create hover state and set alternative fill color
        var hs = polygonTemplate.states.create("hover");
        hs.properties.fill = chart.colors.getIndex(0);

        // Add image series
        var imageSeries = chart.series.push(new am4maps.MapImageSeries());
        imageSeries.mapImages.template.propertyFields.longitude = "longitude";
        imageSeries.mapImages.template.propertyFields.latitude = "latitude";
        

        // Now creating markers on the graph
        // configuring the graph series
        var imageSeries = chart.series.push(new am4maps.MapImageSeries());
        imageSeries.mapImages.template.propertyFields.longitude = "longitude";
        imageSeries.mapImages.template.propertyFields.latitude = "latitude";
        let hover = imageSeries.mapImages.template.states.create("highlight");
        hover.properties.fill = am4core.color("#69975F");
        imageSeries.mapImages.template.tooltipText = "{title}";
        imageSeries.mapImages.template.tooltipPosition="fixed";

        // Marker events
        imageSeries.mapImages.template.events.on("hit", function(ev){
            var data = ev.target._dataItem.dataContext;
            FlowRouter.go("/viewUser?latitude="+ data.latitude.toString() + "&longitude=" + data.longitude.toString());
        })

        imageSeries.mapImages.template.events.on("rightclick", function(ev){
            var data = ev.target._dataItem.dataContext;
            FlowRouter.go("/addRoute?latitude="+ data.latitude.toString() + "&longitude=" + data.longitude.toString());
        })

        // Configure marker and its animation
        var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
        circle.radius = 3;
        circle.propertyFields.fill = "color";
        circle.nonScaling = true;

        var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
        circle2.radius = 3;
        circle2.propertyFields.fill = "color";

        circle2.events.on("inited", function(event){
            animateBullet(event.target);
        })

        // Function to render markers
        function animateBullet(circle) {
            var animation = circle.animate([{ property: "scale", from: 1 / chart.zoomLevel, to: 5 / chart.zoomLevel }, { property: "opacity", from: 1, to: 0 }], 1000, am4core.ease.circleOut);
            animation.events.on("animationended", function(event){
            animateBullet(event.target.object);
            })
        }

        //var colorSet = new am4core.ColorSet();
        imageSeries.data = jsond;

        // Creating line series
        let lineSeries = chart.series.push(new am4maps.MapLineSeries());

        lineSeries.data = lined; 
        lineSeries.mapLines.template.interactionsEnabled = true;
        lineSeries.mapLines.template.line.stroke = am4core.color("#f3fff0");
        lineSeries.mapLines.template.line.strokeOpacity = 0.75;
        lineSeries.mapLines.template.line.strokeWidth = 2;
        //lineSeries.mapLines.template.line.strokeDasharray = "3,3";
        lineSeries.mapLines.template.shortestDistance = false;
        
        // Remove Route 
        lineSeries.events.on("rightclick", function(ev){
          var data = ev.target._dataItem.component._tooltip._dataItem._dataContext;
          FlowRouter.go("/removeRoute?route="+ data._id + "&name="+ data.name);
        });

        lineSeries.mapLines.template.line.tooltipText = "{name}";
        lineSeries.zIndex = 10;
    })
})

// Showlist
// Used to display all the users
Template.showList.helpers({
    userinfos(){
        return Userinfo.find({}, { sort: { createdAt: -1 } })
    },
})

Template.userinfo.onCreated(function(){
    if(this.data.editing){
        Meteor.call('user.setEdit', this.data._id, false);
    }
})


// Userinfo 
// Helper to showlist, to serve individual users
// Also helpers to quick edit
Template.userinfo.events({
  'click .delete'() {
    Meteor.call('user.remove', this._id);
    Meteor.call('neighbour.remove', this._id);
  },
  'click .toggle-edit'(){
    Meteor.call('user.setEdit', this._id, true);
  },
  'click .cancel'(){
    Meteor.call('user.setEdit', this._id, false);
  },
    'submit .edit-user'(event){
        event.preventDefault();
        
        const target = event.target;
        const title = target.title.value;
        const devi = target.device.value;

        Meteor.call('user.update',this._id, title, devi);
        Meteor.call('user.setEdit', this._id, false);
        FlowRouter.go("/showList");
    },   
});


/// UserForm
// To add a new user
Template.userform.onRendered(function(){
    // Removed a lot of code!
})

Template.userform.helpers({
    longitude(){
        return FlowRouter.getQueryParam("longitude");
    },
    latitude (){
        return FlowRouter.getQueryParam("latitude"); 
    },
})
// A lot of brut-force, will optimize
Template.userform.events({
    'submit .new-user'(event){
        event.preventDefault();
        
        const target = event.target;
        const title = target.title.value;
        const devi = target.device.value;
        const longitude = FlowRouter.getQueryParam("longitude");
        const latitude = FlowRouter.getQueryParam("latitude");
        const country = "india";

        Meteor.call('user.insert', title, devi, latitude,longitude, country);
        target.title.value='';
        const dbsear = Userinfo.find({"title": title}).fetch()[0];
        Meteor.call('neighbour.insert', dbsear._id, title, latitude, longitude);
        FlowRouter.go("/");
    },
    'click .delete'(){
        FlowRouter.go("/");
    }
})


/// User Details
// This is used to show details or ViewUSer

Template.userdetail.onRendered(function(){
    var instance = this;
    instance.autorun(function(){
        var  mapdata  = Userinfo.find({latitude: FlowRouter.getQueryParam("latitude"), longitude: FlowRouter.getQueryParam("longitude")}).fetch();
        Session.set("sData", mapdata[0]);
    });
})

Template.userdetail.helpers({
    title(){
        return Session.get("sData").title;
    },
    device(){
        return Session.get("sData").device;
    },
    latitude(){
        return Session.get("sData").latitude;
    },
    longitude(){
        return Session.get("sData").longitude;
    },
})


// To remove the view after we are done.
Template.userdetail.events({
    'click .collaps'(){
        FlowRouter.go("/");
    }
})

// Add route Template
Template.addRoute.helpers({
    dropDown(){
        var data = Userinfo.find({});
        var point = Userinfo.find({latitude: FlowRouter.getQueryParam("latitude"), longitude: FlowRouter.getQueryParam("longitude")}).fetch();
        var neData = [];
        data.forEach((post) =>{
            if(post.longitude === point[0].longitude && post.latitude === point[0].latitude){
                Session.set("addData", post._id);
            }
            else{
                if(NeighbourInfo.find({$and: [{userID: point[0]._id},{ neighbour: post._id}]}).count()){
                }else{
                    neData.push(post);
                }
            } 
        });
        return neData;
    },
})

Template.addRoute.events({
    'click #routeDest'(event){
        if(event.target.value){
            // Verrification
            //Method 1:
            // if(NeighbourInfo.find({userID: userID}).fetch()[0].neighbour.includes(neighbourID)){
            // Method 2:
            if(NeighbourInfo.find({$and: [{userID: Session.get("addData")},{ neighbour: event.target.value}]}).count()){
            }else{
                Meteor.call('neighbour.add', Session.get("addData"), event.target.value);
                Meteor.call('neighbour.add',event.target.value, Session.get("addData"));
            }
            FlowRouter.go("/");
        }
    },
    'contextmenu': function(e) {
        return false;
    },
    'click .cancelAdd'(){
        FlowRouter.go("/");
    }
})


// Remove Route template
Template.removeRoute.onCreated(function(){
    this.autorun(function(){
        
    })
})
Template.removeRoute.events({
    'click .Remove'(){
        var idstring = FlowRouter.getQueryParam("route");
        var id1 = idstring.slice(0,17);
        var id2 = idstring.slice(17,34);
        Meteor.call('neighbour.removeNeighbour', id1, id2);
        FlowRouter.go("/");
    },
    'click .cancelRemove'(){
        FlowRouter.go("/");
    }
})
Template.removeRoute.helpers({
    name(){
        return FlowRouter.getQueryParam("name");
    },
})