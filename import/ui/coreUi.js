import  './coreUi.html'; // importring html to add scripts
import './world.js'; // importing map feature to port

// coreUI dependecies <--- not required -->
import '@coreui/coreui/dist/js/coreui.bundle.min';
import 'simplebar/dist/simplebar.min';
import '@coreui/utils/dist/coreui-utils';


import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';

import './login.js';
import './register.js';


import { Template } from 'meteor/templating';

Template.trial.onCreated(function(){
    this.sidebarState = new ReactiveVar(true);
    this.asidebarState = new ReactiveVar(false);
})

Template.trial.helpers({
    getUser(){
        return Meteor.user();
    },
    sidebarState: function(){
        return Template.instance().sidebarState.get();
    },
    asidebarState: function(){
        return Template.instance().asidebarState.get();
    }
})

Template.trial.events({
    'click .logOut'(){
        Accounts.makeClientLoggedOut();
        FlowRouter.go('/');
    },
    'click .logIn'(){
        FlowRouter.go('/login');
    },
    'click .sidebarToggle'(){
        if(Template.instance().sidebarState.get()){
            Template.instance().sidebarState.set( false );
        }
        else{
            Template.instance().sidebarState.set( true );
        }
        // Jquery way !!
        // $('.ui-sidebar').sidebar('toggle');
    },
    'click .asidebarToggle'(){
        if(Template.instance().asidebarState.get()){
            Template.instance().asidebarState.set( false );
        }
        else{
            Template.instance().asidebarState.set( true );
        }
        //$('.aside-sidebar').sidebar('toggle');
    },
    'click #lightthemerBtn'(event, temp) {
        //temp.$('#themer').removeClass('dark-theme');
        $('body').removeClass('dark-theme');
        console.log("light themer pressed");
    },
    'click #darkthemerBtn'(event, temp) {
        //temp.$('#themer').addClass('dark-theme');
        $('body').addClass('dark-theme');
        console.log("dark themer pressed");
    }
})