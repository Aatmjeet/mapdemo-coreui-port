import './login.html';

Template.login.events({
    'submit form': function (event) {
        console.log("login pressed");
        event.preventDefault();
        var email = event.target.email.value;
        var password = event.target.password.value;
        Meteor.loginWithPassword(email, password, function (error) {
            if (error) {
                return swal({
                    title: "Email or password incorrect",
                    text: "please try again",
                    timer: 8000,
                    showConfirmbutton: false,
                    type: "error"
                });
            } else {
                FlowRouter.go('/');
            }
        });
        return false;
    },
    'click .goToRegister'(){
        FlowRouter.go('/register');
    }
})