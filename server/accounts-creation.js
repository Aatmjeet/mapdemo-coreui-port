import { Accounts } from "meteor/accounts-base";

Accounts.onCreateUser(function(options, user){
    user.profile = options.profile || {};

    user.profile.firstName = options.firstName;
    user.profile.lastName = options.lastName;

    // Basic Role Setup
    user.roles = ["User"];

    // Basic Organization Setup
    user.profile.organization = ["Org"];
    
    return user;
})