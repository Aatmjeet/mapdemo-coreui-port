FlowRouter.route('/', {
    name: 'default',
    action(){
        BlazeLayout.render("trial");
    }
});

FlowRouter.route('/login',{
    name: 'login',
    action(){
        BlazeLayout.render("login");
    }
});

FlowRouter.route('/register',{
    name: 'register',
    action(){
        BlazeLayout.render("register");
    }
});

FlowRouter.route('/search', {
    name: 'search',
    action(){
        BlazeLayout.render("trial", {main:"search"});
    }
});

FlowRouter.route('/showList', {
    name: 'list',
    action(){
        BlazeLayout.render("trial", {main:"showList"});
    }
});

FlowRouter.route('/addRoute', {
    name: 'addRoute',
    action: function(params, queryParams){
        BlazeLayout.render("trial", {main:"addRoute"});
    }
});

FlowRouter.route('/removeRoute', {
    name: 'addRoute',
    action: function(params, queryParams){
        BlazeLayout.render("trial", {main:"removeRoute"});
    }
});

FlowRouter.route("/addUser",{
    name: 'addwithParams',
    action: function(params, queryParams){
        BlazeLayout.render("trial", {main:'userform'});
    }
})

FlowRouter.route("/viewUser",{
    name: 'viewwithParams',
    action: function(params, queryParams){
        BlazeLayout.render("trial", {main:'userdetail'});
    }
})

FlowRouter.route('/*', {
    name: 'unknownRoute',
    action() {
        BlazeLayout.render("unknownRoute");
    }
});

// Exposed FlowRouter to console for development! Will remove!
// this.FlowRouter = FlowRouter;